CNT=0
while IFS= read line; do
	if [ $((CNT%2)) -eq 0 ]; then
		echo "# running: $line"
	fi
	if [ $((CNT%2)) -ne 0 ]; then
		echo "$line" | $1 2>&1 | head -n1
		echo
	fi
	CNT=$(expr $CNT + 1)
done < $2
