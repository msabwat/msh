/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   minishell.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 19:59:05 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/11 02:19:39 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MINISHELL_H
# define MINISHELL_H

# include "libft.h"

# define PATHMAX 4096
# define MAXSTACK 63357

typedef enum	e_tkn_type {
	UNKNOWN = 0,
	SEMICOLON,
	PIPE,
	OUTREDIR,
	INREDIR,
	EOFREDIR,
	SPACE = -4,
	WORD = -3,
}				t_tkn_type;

typedef struct	s_token {
	t_tkn_type	type;
	size_t		start;
	size_t		end;
}				t_token;

typedef struct	s_cmd {
	char	**cmd_argv;
	int		in;
	int		out;
	int		pipe1[2];
	int		pipe2[2];
	int		ret;
}				t_cmd;

typedef struct	s_env {
	char	*name;
	char	*value;
}				t_env;

typedef struct	s_sys {
	t_list	*lstenv;
	t_list	*lstcmd;
	t_stack	*stackids;
	t_stack	*pipes;
	int		ret;
	int		state;
	int		p_ret;
}				t_sys;

typedef struct	s_indexes {
	char	*str;
	int		start;
	int		end;
}				t_indexes;

t_token			get_next_token(char **input, int *start_index, int *stop_index);
int				parse (t_sys *sys, char *input);
char			*str_from_tkn(t_sys *sys, t_token tkn, char *input);
char			**update_str(t_sys *sys, t_list *cur, t_token tkn, char *input);
void			handle_tokenizer_cur(char **cur, int *index);

int				is_separator(char c);
int				handle_token1(t_token token, t_token cur_token);
int				validate_token(t_token tkn, t_token n_tkn);
t_token			init_tkn(t_tkn_type tkn, int start, int end);
void			handle_matched_quote(char **input, int *index);

void			add_cmd(t_sys *sys, int pipefd[2], t_list *cur);
t_cmd			*init_cmd(t_sys *sys);
void			handle_inredir(t_list **head, char *filename);
void			handle_outredir(t_list **head, char *filename
							, int flag, t_token tkn);
void			handle_semicolon(t_sys *sys, t_token n_tkn, char *input);
void			handle_pipe(t_sys *sys, t_token n_tkn, char *input);
void			handle_redir(t_sys *sys, t_token tkn,
								t_token n_tkn, char *input);
void			handle_cmd(t_sys *sys, t_token n_tkn, char *input);
void			handle_wait(t_sys *sys, t_cmd *cmd, int id);

int				handle_expansion(t_sys *sys, t_cmd *cmd);
void			new_from_dbquote(t_sys *sys, char *str, char **new, int *index);
void			new_from_squote(t_sys *sys, char *str, char **new, int *index);
void			ret_from_dollar(t_sys *sys, char **new,
								t_indexes indexes, int *index);
void			env_from_dollar(t_sys *sys, char **new,
								t_indexes indexes, int *index);
void			expand_dollar(t_sys *sys, char **new, int s);

int				execute (t_sys *sys);
char			*test_path(int *ret, char **paths, char *argv);
void			fill_env_str(t_list *cur, char ***res);
char			**str_from_list(t_sys *sys, t_list **lstenv);
char			*getpath(t_list **lstenv, char **argv);
int				launch(t_sys *sys, t_cmd *cmd, char *path, char **env);

void			handle_sigquit(int sig);
void			handle_sigint(int sig);

void			execute_echo(t_cmd *cmd, char **options);
void			execute_cd(t_cmd *cmd, t_list **lstenv, char **argv);
void			execute_pwd(t_sys *sys, t_cmd *cmd);
void			execute_env(t_cmd *cmd, t_list **head);
void			execute_export(t_cmd *cmd, t_sys *sys, char *argument);
void			execute_unset(t_list **lstenv, char *name, t_cmd *cmd);
void			execute_exit(t_sys *sys, t_cmd *cmd, int ret, char **options);

void			set_value_name(t_sys *sys, char **arg);
void			set_value_null(t_sys *sys, char **arg);
void			handle_export_err(t_cmd *cmd, char **arg);

t_list			*init_env(t_sys *sys, char **env);
char			*get_shenv(t_list **lstenv, char *env);
void			set_shenv(t_sys*sys, t_list **lstenv, char *name, char *value);
void			print_lstenv(t_list *cur, t_cmd *cmd);
void			free_env(void *node, size_t s);
void			free_cmd(void *node, size_t s);
void			free_paths(char **paths);
void			free_lstenv(t_list *cur);
void			free_tmp_env(char **env);
void			cleanup_shell(t_sys *sys, int ret);
char			*debug_token (t_tkn_type tkn);
void			exit_with_error(t_sys *sys, int err);
void			handle_redir_error(t_list *cur);
#endif
