/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_table_utils.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:11:56 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/11 02:15:00 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <errno.h>

void	add_cmd(t_sys *sys, int pipefd[2], t_list *cur)
{
	t_cmd	*cmd;

	cmd = malloc(sizeof(t_cmd));
	if (!cmd)
		exit_with_error(sys, ENOMEM);
	cmd->cmd_argv = NULL;
	cmd->in = pipefd[0];
	cmd->out = 1;
	cmd->pipe1[0] = ((t_cmd*)cur->content)->pipe1[1];
	cmd->pipe1[1] = ((t_cmd*)cur->content)->pipe1[0];
	cmd->pipe2[0] = pipefd[0];
	cmd->pipe2[1] = pipefd[1];
	cmd->ret = 0;
	ft_lstadd(&cur, ft_lstnew((t_cmd*)cmd, sizeof(t_cmd)));
}

t_cmd	*init_cmd(t_sys *sys)
{
	t_cmd	*cmd;

	cmd = malloc(sizeof(t_cmd));
	if (!cmd)
		exit_with_error(sys, ENOMEM);
	cmd->cmd_argv = NULL;
	cmd->in = 0;
	cmd->out = 1;
	cmd->pipe1[0] = -42;
	cmd->pipe1[1] = -42;
	cmd->pipe2[0] = -42;
	cmd->pipe2[1] = -42;
	cmd->ret = 0;
	return (cmd);
}

void	handle_outredir(t_list **head, char *filename, int flag, t_token tkn)
{
	t_list		*cur;
	struct stat	buf;

	cur = *head;
	while (cur->next != NULL)
		cur = cur->next;
	if (stat(filename, &buf) != -1)
	{
		if (((buf.st_mode & S_IFMT) == S_IFREG) && ((buf.st_mode & S_IWUSR)))
		{
			if (tkn.type == EOFREDIR)
				flag = O_WRONLY | O_APPEND;
			else if (tkn.type == OUTREDIR)
				flag = O_WRONLY;
			((t_cmd*)cur->content)->out = open(filename, flag, 0644);
		}
		else
			handle_redir_error(cur);
	}
	else
	{
		((t_cmd*)cur->content)->out = open(filename, flag, 0644);
		if (((t_cmd*)cur->content)->out < 0)
			handle_redir_error(cur);
	}
}

void	handle_inredir(t_list **head, char *filename)
{
	t_list		*cur;
	t_cmd		*node;
	struct stat	buf;

	cur = *head;
	node = ((t_cmd*)cur->content);
	while ((node->pipe1[0] == -42) && ((node->pipe1[1] == -42))
			&& (cur->next != NULL))
		cur = cur->next;
	if ((stat(filename, &buf) != -1)
		&& (((buf.st_mode & S_IFMT) == S_IFREG) && (buf.st_mode & S_IRUSR)))
		((t_cmd*)cur->content)->in = open(filename, O_RDONLY);
	else
	{
		((t_cmd*)cur->content)->in = -42;
		ft_putendl_fd("could not redirect from file: not found or invalid", 2);
	}
}
