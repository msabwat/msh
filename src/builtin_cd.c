/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_cd.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 01:49:28 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 12:18:02 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <errno.h>

static int	is_errno(void)
{
	if (errno == EFAULT)
		return (1);
	else if (errno == EIO)
		return (1);
	else if (errno == ELOOP)
		return (1);
	else if (errno == ENAMETOOLONG)
		return (1);
	else if (errno == ENOMEM)
		return (1);
	return (0);
}

static void	print_error(char *path, int fd, char *msg)
{
	ft_putstr_fd("cd : ", fd);
	ft_putstr_fd(path, fd);
	ft_putendl_fd(msg, fd);
}

static void	handle_error(char *path, t_cmd *cmd)
{
	if (errno == EACCES)
		print_error(path, 2, ": Permission denied");
	else if (errno == ENOENT)
		print_error(path, 2, ": No such file or directory");
	else if (errno == ENOTDIR)
		print_error(path, 2, ": Not a directory");
	else if (is_errno())
		print_error(path, 2, ": Error");
	cmd->ret = 1;
}

static void	handle_cd(t_cmd *cmd, t_list **lstenv, char *path)
{
	char			*new_path;
	int				ret;

	ret = 0;
	new_path = NULL;
	if (cmd->in != 0)
		return ;
	if (path == NULL)
	{
		new_path = get_shenv(lstenv, "HOME");
		if (!new_path)
		{
			ft_putendl_fd("minishell: cd: HOME not set", 2);
			cmd->ret = 1;
			return ;
		}
		path = new_path;
	}
	ret = chdir(path);
	if (ret == 0)
		cmd->ret = 0;
	else
		handle_error(path, cmd);
}

void		execute_cd(t_cmd *cmd, t_list **lstenv, char **argv)
{
	if (argv[2] != NULL)
	{
		ft_putendl_fd("minishell: cd: too many arguments", cmd->out);
		cmd->ret = 1;
	}
	else
		handle_cd(cmd, lstenv, argv[1]);
}
