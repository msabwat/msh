/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_squote.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/09 20:11:01 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 13:05:55 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <errno.h>

static t_indexes	handle_indexes_s(int *index, char *str, char quote)
{
	int		end;
	int		start;
	int		len;

	start = *index + 1;
	end = start;
	len = (int)ft_strlen(str);
	while (end < len)
	{
		if (str[end] == quote)
			break ;
		end++;
	}
	if (str[end] != quote)
		*index = -42;
	else
		*index = end;
	return ((t_indexes){ .start=start, .end=end});
}

static char			*new_sqstr(t_sys *sys, char *str, t_indexes ind, char quote)
{
	char	*new;
	int		i;
	int		j;
	int		size;

	i = ind.start;
	j = 0;
	size = ind.end - ind.start;
	new = ft_strnew(size);
	if (!new)
		exit_with_error(sys, ENOMEM);
	while ((i < (int)ft_strlen(str) - 1) && (j < size))
	{
		if (str[i] == quote)
			i++;
		new[j++] = str[i++];
	}
	return (new);
}

static char			*append_sqstr(t_sys *sys, char **n, t_indexes ind, char q)
{
	char	*tmp;
	char	*tofree;
	int		i;
	int		j;
	int		size;

	tofree = *n;
	i = ind.start;
	j = 0;
	size = ind.end - ind.start;
	tmp = ft_strnew(size);
	if (!tmp)
		exit_with_error(sys, ENOMEM);
	while ((i < (int)ft_strlen(ind.str) - 1) && (j < size))
	{
		if ((ind.str)[i] == q)
			i++;
		tmp[j++] = (ind.str)[i++];
	}
	*n = ft_strjoin(tofree, tmp);
	if (!(*n))
		exit_with_error(sys, ENOMEM);
	return (tmp);
}

void				new_from_squote(t_sys *sys, char *str, char **new, int *i)
{
	char		*tofree;
	char		*tmp;
	char		quote;
	t_indexes	ind;

	quote = str[*i];
	tofree = NULL;
	tmp = NULL;
	ind = handle_indexes_s(i, str, quote);
	ind.str = str;
	if (*i == -42)
		return ;
	if (!(*new))
		*new = new_sqstr(sys, str, ind, quote);
	else
	{
		tofree = *new;
		tmp = append_sqstr(sys, new, ind, quote);
		ft_strdel(&tofree);
		ft_strdel(&tmp);
	}
}
