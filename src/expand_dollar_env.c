/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_dollar_env.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/02 23:33:08 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 11:58:23 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <errno.h>

static void	a_envxp_strt(t_sys *sys, t_indexes indexes, char **new, int *index)
{
	char *tofree;

	tofree = *new;
	if (indexes.end == (int)ft_strlen(*new) - 1)
		*new = NULL;
	else
	{
		*new = ft_strsub(*new, indexes.end + 1,
			ft_strlen(*new) - indexes.end - 1);
		if (!(*new))
			exit_with_error(sys, ENOMEM);
	}
	ft_strdel(&tofree);
	*index = -1;
}

static void	alloc_env_exp(t_sys *sys, t_indexes indexes, char **new, int *index)
{
	char *tofree;
	char *post;
	char *pre;

	tofree = *new;
	post = NULL;
	pre = NULL;
	if (indexes.start == 1)
		a_envxp_strt(sys, indexes, new, index);
	else
	{
		pre = ft_strsub(*new, 0, indexes.start - 1);
		if (!pre)
			exit_with_error(sys, ENOMEM);
		post = ft_strsub(*new, indexes.end + 1,
			ft_strlen(*new) - indexes.end - 1);
		if (!(post))
			exit_with_error(sys, ENOMEM);
		*new = ft_strjoin(pre, post);
		ft_strdel(&tofree);
		ft_strdel(&pre);
		ft_strdel(&post);
		*index = indexes.end;
	}
}

static void	expand_env(t_sys *sys, char **new, t_indexes indexes, char *post)
{
	char *tofree;
	char *pre;

	tofree = *new;
	pre = NULL;
	if (indexes.end == (int)ft_strlen(*new))
	{
		*new = post;
		ft_strdel(&tofree);
	}
	else
	{
		pre = ft_strsub(*new, indexes.end + 1,
			ft_strlen(*new) - indexes.end - 1);
		if (!pre)
			exit_with_error(sys, ENOMEM);
		*new = ft_strjoin(post, pre);
		if (!(*new))
			exit_with_error(sys, ENOMEM);
		ft_strdel(&tofree);
		ft_strdel(&post);
		ft_strdel(&pre);
	}
}

static char	*get_env(t_sys *sys, char **new, t_indexes indexes, int *index)
{
	char *tmp;
	char *name;

	tmp = NULL;
	name = ft_strsub(*new, indexes.start, indexes.end - indexes.start + 1);
	if (!name)
		exit_with_error(sys, ENOMEM);
	tmp = get_shenv(&(sys->lstenv), name);
	ft_strdel(&name);
	if (!tmp)
	{
		alloc_env_exp(sys, indexes, new, index);
		return (NULL);
	}
	return (tmp);
}

void		env_from_dollar(t_sys *sys, char **new, t_indexes ind, int *index)
{
	char	*tmp;
	char	*post;
	char	*pre;

	post = NULL;
	pre = NULL;
	if (!(tmp = get_env(sys, new, ind, index)))
		return ;
	if (ind.start > 1)
	{
		pre = ft_strsub(*new, 0, ind.start - 1);
		if (!pre)
			exit_with_error(sys, ENOMEM);
		post = ft_strjoin(pre, tmp);
		if (!(post))
			exit_with_error(sys, ENOMEM);
		ft_strdel(&pre);
		*index = ft_strlen(post) - 1;
	}
	else
	{
		post = ft_strdup(tmp);
		*index = ft_strlen(post) - 1;
	}
	expand_env(sys, new, ind, post);
}
