/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_utils.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/10 14:11:48 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 14:14:20 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"

static void	handle_string(t_sys *sys, char **new, int *index)
{
	int start;
	int end;

	start = 0;
	end = 0;
	if ((*new)[*index] == '?')
	{
		start = *index;
		end = *index;
		ret_from_dollar(sys, new, (t_indexes){.start=start, .end=end}, index);
	}
	else if ((ft_isalnum((*new)[*index]) || (*new)[*index] == '_'))
	{
		start = *index;
		while (ft_isalnum((*new)[*index]) || (*new)[*index] == '_')
			(*index)++;
		end = *index - 1;
		env_from_dollar(sys, new, (t_indexes){.start=start, .end=end}, index);
	}
	else if ((*new)[*index] == '$')
		(*index) -= 1;
}

void		expand_dollar(t_sys *sys, char **new, int s)
{
	int index;
	int size;

	index = 0;
	size = (int)ft_strlen(*new);
	while (index < size)
	{
		if (((*new)[index] == '$') && (index >= s - 2))
		{
			index += 1;
			handle_string(sys, new, &index);
		}
		index += 1;
		if (!(*new))
			return ;
		size = (int)ft_strlen(*new);
	}
}
