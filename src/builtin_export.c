/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_export.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:04:20 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/11 02:19:31 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <errno.h>

static t_env	*alloc_env(t_sys *sys, t_list *cur)
{
	t_env	*node;

	node = malloc(sizeof(t_env));
	if (!node)
		exit_with_error(sys, ENOMEM);
	node->name = ((t_env*)cur->content)->name;
	node->value = ((t_env*)cur->content)->value;
	return (node);
}

static t_list	*init_lstenv(t_sys *sys, t_list *cur)
{
	t_env	*node;
	t_list	*copy;
	t_list	*head_copy;

	copy = NULL;
	head_copy = NULL;
	node = NULL;
	while (cur)
	{
		node = alloc_env(sys, cur);
		if (!copy)
		{
			copy = ft_lstnew(node, sizeof(t_env));
			head_copy = copy;
		}
		else
		{
			ft_lstadd(&copy, ft_lstnew(node, sizeof(t_env)));
			copy = copy->next;
		}
		cur = cur->next;
	}
	return (head_copy);
}

static void		sort_lstenv(t_list *head_copy)
{
	t_env	*node;
	t_env	*n_node;
	t_list	*cur;
	t_list	*next;
	size_t	len;

	cur = head_copy;
	node = NULL;
	n_node = NULL;
	next = NULL;
	len = ft_lstlen(cur);
	while (len)
	{
		cur = head_copy;
		while (cur->next != NULL)
		{
			next = cur->next;
			node = ((t_env*)cur->content);
			n_node = ((t_env*)next->content);
			if (ft_strcmp(node->name, n_node->name) > 0)
				ft_lstswap(cur, next);
			cur = cur->next;
		}
		len--;
	}
}

static void		handle_split(t_sys *sys, char *argument, t_cmd *cmd)
{
	char	**arg;
	char	**tmp;
	int		size;

	size = 0;
	arg = ft_strsplit(argument, '=');
	tmp = arg;
	if (tmp)
	{
		while (*tmp)
		{
			size++;
			tmp++;
		}
		if (size == 2)
			set_value_name(sys, arg);
		else if (size == 1)
			set_value_null(sys, arg);
		else
			handle_export_err(cmd, arg);
	}
}

void			execute_export(t_cmd *cmd, t_sys *sys, char *argument)
{
	t_list	*head_copy;

	head_copy = NULL;
	if (cmd->in != 0)
		return ;
	if (argument == NULL)
	{
		head_copy = init_lstenv(sys, *&(sys->lstenv));
		sort_lstenv(head_copy);
		print_lstenv(head_copy, cmd);
		free_lstenv(head_copy);
	}
	else
	{
		if (ft_strchr(argument, '='))
			handle_split(sys, argument, cmd);
	}
	if (cmd->pipe1[1] != 42)
		close(cmd->pipe1[1]);
}
