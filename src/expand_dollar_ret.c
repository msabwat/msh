/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_dollar_ret.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/09 19:41:45 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 12:24:19 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <errno.h>

static void	expand_ret(t_sys *sys, char **new, t_indexes indexes, char *post)
{
	char *pre;
	char *tofree;

	tofree = *new;
	pre = NULL;
	if (indexes.end == (int)ft_strlen(*new) - 1)
	{
		*new = post;
		ft_strdel(&tofree);
	}
	else
	{
		pre = ft_strsub(*new, indexes.end + 1,
			ft_strlen(*new) - indexes.end - 1);
		if (!pre)
			exit_with_error(sys, ENOMEM);
		*new = ft_strjoin(post, pre);
		if (!(*new))
			exit_with_error(sys, ENOMEM);
		ft_strdel(&tofree);
		ft_strdel(&post);
		ft_strdel(&pre);
	}
}

void		ret_from_dollar(t_sys *sys, char **new, t_indexes ind, int *i)
{
	char	*tmp;
	char	*post;
	char	*pre;

	post = NULL;
	pre = NULL;
	tmp = ft_itoa(sys->ret);
	if (ind.start - 1 > 0)
	{
		pre = ft_strsub(*new, 0, ind.start - 1);
		if (!pre)
			exit_with_error(sys, ENOMEM);
		post = ft_strjoin(pre, tmp);
		if (!(post))
			exit_with_error(sys, ENOMEM);
		ft_strdel(&pre);
		ft_strdel(&tmp);
	}
	else
		post = tmp;
	*i = ft_strlen(post) - 1;
	expand_ret(sys, new, ind, post);
}
