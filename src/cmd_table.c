/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   cmd_table.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:11:40 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 12:49:23 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <errno.h>

void	handle_semicolon(t_sys *sys, t_token n_tkn, char *input)
{
	t_list	*cur;
	t_cmd	*cmd;

	cur = *(&(sys->lstcmd));
	while (cur->next != NULL)
		cur = cur->next;
	cmd = malloc(sizeof(t_cmd));
	if (!cmd)
		exit_with_error(sys, ENOMEM);
	cmd->cmd_argv = malloc(sizeof(char *) * 3);
	if (!cmd->cmd_argv)
		exit_with_error(sys, ENOMEM);
	cmd->cmd_argv[2] = NULL;
	cmd->cmd_argv[1] = NULL;
	cmd->cmd_argv[0] = str_from_tkn(sys, n_tkn, input);
	cmd->in = 0;
	cmd->out = 1;
	cmd->pipe1[0] = -42;
	cmd->pipe1[1] = -42;
	cmd->pipe2[0] = -42;
	cmd->pipe2[1] = -42;
	cmd->ret = 0;
	ft_lstadd(&cur, ft_lstnew((t_cmd*)cmd, sizeof(t_cmd)));
}

void	handle_pipe(t_sys *sys, t_token n_tkn, char *input)
{
	t_list	*cur;
	int		status;
	int		pipefd[2];

	status = pipe(pipefd);
	if (status == -1)
		exit_with_error(sys, errno);
	ft_stackpush(sys->pipes, pipefd[0]);
	ft_stackpush(sys->pipes, pipefd[1]);
	cur = *(&(sys->lstcmd));
	while (cur->next != NULL)
		cur = cur->next;
	((t_cmd*)cur->content)->out = pipefd[1];
	((t_cmd*)cur->content)->pipe1[1] = pipefd[1];
	((t_cmd*)cur->content)->pipe1[0] = pipefd[0];
	add_cmd(sys, pipefd, cur);
	cur = *(&(sys->lstcmd));
	while (cur->next != NULL)
		cur = cur->next;
	((t_cmd*)cur->content)->cmd_argv = malloc(sizeof(char *) * 3);
	if (!((t_cmd*)cur->content)->cmd_argv)
		exit_with_error(sys, ENOMEM);
	((t_cmd*)cur->content)->cmd_argv[2] = NULL;
	((t_cmd*)cur->content)->cmd_argv[1] = NULL;
	((t_cmd*)cur->content)->cmd_argv[0] = str_from_tkn(sys, n_tkn, input);
}

void	handle_redir(t_sys *sys, t_token tkn, t_token n_tkn, char *input)
{
	char		*filename;
	int			flag;

	flag = O_CREAT | O_WRONLY;
	filename = str_from_tkn(sys, n_tkn, input);
	if ((tkn.type == OUTREDIR) || (tkn.type == EOFREDIR))
		handle_outredir(&(sys->lstcmd), filename, flag, tkn);
	else if (tkn.type == INREDIR)
		handle_inredir(&(sys->lstcmd), filename);
	ft_strdel(&filename);
}

void	handle_cmd(t_sys *sys, t_token tkn, char *input)
{
	t_list	*cur;
	t_cmd	*node;

	cur = *(&(sys->lstcmd));
	while (cur->next != NULL)
		cur = cur->next;
	node = ((t_cmd*)cur->content);
	if (!(node->cmd_argv))
	{
		node->cmd_argv = malloc(sizeof(char *) * 3);
		if (!(node->cmd_argv))
			exit_with_error(sys, ENOMEM);
		node->cmd_argv[2] = NULL;
		node->cmd_argv[1] = NULL;
		node->cmd_argv[0] = str_from_tkn(sys, tkn, input);
	}
	else
	{
		if (!(node->cmd_argv[1]))
			node->cmd_argv[1] = str_from_tkn(sys, tkn, input);
		else
		{
			node->cmd_argv = update_str(sys, cur, tkn, input);
		}
	}
}
