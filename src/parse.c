/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   parse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/23 23:08:56 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 14:22:34 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>

static int	check_piperedir(t_tkn_type tkn, int is_redir)
{
	if ((tkn == PIPE) && (!is_redir || is_redir == 2))
		return (1);
	else if ((tkn == OUTREDIR) && (is_redir))
		return (1);
	else if ((tkn == INREDIR) && (is_redir))
		return (1);
	else if ((tkn == EOFREDIR) && (is_redir))
		return (1);
	return (0);
}

int			validate_token(t_token tkn, t_token n_tkn)
{
	if ((n_tkn.type == INREDIR) && (tkn.type == WORD))
		return (1);
	else if ((tkn.type == INREDIR) && (n_tkn.type != WORD))
		return (0);
	if (((tkn.type == UNKNOWN) && (n_tkn.type == PIPE))
		|| ((tkn.type == UNKNOWN) && (n_tkn.type == SEMICOLON)))
		return (0);
	if (((n_tkn.type != WORD) && (tkn.type == OUTREDIR))
		|| ((n_tkn.type != WORD) && (tkn.type == EOFREDIR)))
		return (0);
	if (n_tkn.type == SEMICOLON && tkn.type != WORD)
		return (0);
	return (1);
}

static void	update_table(t_sys *sys, t_token tkn, t_token n_tkn, char *input)
{
	t_list	*cur;

	cur = *(&(sys->lstcmd));
	if (!cur)
	{
		*(&(sys->lstcmd)) = ft_lstnew(init_cmd(sys), sizeof(t_cmd));
		if (!(*(&(sys->lstcmd))))
			exit_with_error(sys, ENOMEM);
	}
	if ((tkn.type == SEMICOLON) && (n_tkn.type == WORD))
		handle_semicolon(sys, n_tkn, input);
	else if ((tkn.type == PIPE) && (n_tkn.type == WORD))
		handle_pipe(sys, n_tkn, input);
	else if (check_piperedir(tkn.type, 1) && (n_tkn.type == WORD))
		handle_redir(sys, tkn, n_tkn, input);
	else if (n_tkn.type == WORD)
		handle_cmd(sys, n_tkn, input);
}

int			handle_token2(t_token token, t_token cur_token)
{
	if (cur_token.type != SPACE)
	{
		if (check_piperedir(cur_token.type, 2))
			return (0);
	}
	else
	{
		if (check_piperedir(token.type, 2))
			return (0);
	}
	return (1);
}

int			parse(t_sys *sys, char *input)
{
	t_token	token;
	t_token	cur_token;
	int		start_index;
	int		stop_index;
	char	*str;

	start_index = 0;
	stop_index = 0;
	token = ((t_token){.type = UNKNOWN, .start = 0, .end = 0});
	cur_token = ((t_token){.type = UNKNOWN, .start = 0, .end = 0});
	str = input;
	while (*input)
	{
		if (cur_token.type != SPACE)
			token = cur_token;
		start_index = stop_index;
		cur_token = get_next_token(&input, &start_index, &stop_index);
		if (!handle_token1(token, cur_token))
			return (0);
		update_table(sys, token, cur_token, str);
	}
	if (!handle_token2(token, cur_token))
		return (0);
	return (1);
}
