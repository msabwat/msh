/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_echo.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:03:13 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 00:37:17 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

void	execute_echo(t_cmd *cmd, char **options)
{
	int		index;
	int		flag;

	flag = 0;
	index = 1;
	if (options[index])
	{
		if (ft_strcmp(options[index], "-n") == 0)
		{
			index += 1;
			flag = 1;
		}
		if (options[index])
			ft_putstr_fd(options[index++], cmd->out);
	}
	while (options[index])
	{
		ft_putstr_fd(" ", cmd->out);
		ft_putstr_fd(options[index++], cmd->out);
	}
	if (flag != 1)
		ft_putstr_fd("\n", cmd->out);
	if (cmd->pipe1[1] != 42)
		close(cmd->pipe1[1]);
}
