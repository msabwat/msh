/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:13:35 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 12:58:00 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <sys/stat.h>

char			*test_path(int *ret, char **paths, char *arg)
{
	struct stat	buffer;
	char		*tmp;
	char		*tofree;
	char		*path;

	tmp = NULL;
	path = arg;
	tofree = tmp;
	if (ft_strlen(path) == 0)
		return ("");
	while (*paths)
	{
		tmp = ft_strjoin(*paths, "/");
		tofree = tmp;
		path = ft_strjoin(tmp, arg);
		ft_strdel(&tofree);
		*ret = stat(path, &buffer);
		if (*ret == 0)
			break ;
		ft_strdel(&path);
		paths++;
	}
	return (path);
}

static size_t	handle_len(t_list *cur)
{
	size_t	len;

	len = 0;
	if (((t_env*)cur->content)->value != NULL)
		len = ft_strlen(((t_env*)cur->content)->value);
	return (len);
}

void			fill_env_str(t_list *cur, char ***res)
{
	size_t		index;
	int			j;
	char		**tmp;
	size_t		len;

	index = 0;
	j = 0;
	tmp = *res;
	len = 0;
	while (cur)
	{
		len = handle_len(cur);
		**res = ft_strnew((ft_strlen(((t_env*)cur->content)->name) + len));
		j = 0;
		index = 0;
		while (index < ft_strlen(((t_env*)cur->content)->name))
			(**res)[index++] = (((t_env*)cur->content)->name)[j++];
		(**res)[index++] = '=';
		j = 0;
		while (index < (ft_strlen(((t_env*)cur->content)->name) + len))
			(**res)[index++] = ((t_env*)cur->content)->value[j++];
		cur = cur->next;
		(*res)++;
	}
	*res = tmp;
}
