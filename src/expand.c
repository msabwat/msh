/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/02 21:08:30 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 14:14:13 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <errno.h>

char		*new_in_quotes(t_sys *sys, int start, int size, char *str)
{
	char	*new;
	int		i;
	int		j;

	i = start;
	j = 0;
	new = ft_strnew(size);
	if (!new)
		exit_with_error(sys, ENOMEM);
	while (i < (int)ft_strlen(str) && (j < size))
	{
		if ((str[i] == '\\') && (str[i + 1] == '\"'))
			i++;
		new[j++] = str[i++];
	}
	return (new);
}

void		append_in_quotes(t_sys *sys, char **new, t_indexes ind, char *str)
{
	char	*tofree;
	char	*tmp;
	int		size;
	int		i;
	int		j;

	i = ind.start;
	j = 0;
	size = ind.end - ind.start;
	tofree = *new;
	tmp = ft_strnew(size);
	if (!tmp)
		exit_with_error(sys, ENOMEM);
	while ((i < (int)ft_strlen(str)) && (j < size))
	{
		if ((str[i] == '\\') && (str[i + 1] == '\"'))
			i++;
		tmp[j++] = str[i++];
	}
	*new = ft_strjoin(tofree, tmp);
	if (!(*new))
		exit_with_error(sys, ENOMEM);
	ft_strdel(&tofree);
	ft_strdel(&tmp);
}

static void	append_new_quotes(t_sys *sys, char *str, char **new, int *index)
{
	t_indexes	ind;
	int			size;

	size = 0;
	ind.start = *index;
	ind.end = ind.start;
	while (ind.end < (int)ft_strlen(str))
	{
		if ((str[ind.end] == '\'') || (str[ind.end] == '\"'))
			break ;
		if ((str[ind.end] == '\\') && (str[ind.end + 1] == '\"'))
			ind.end += 1;
		(ind.end)++;
	}
	*index = ind.end - 1;
	size = ind.end - ind.start;
	if (size == 0)
		return ;
	if (!(*new))
		*new = new_in_quotes(sys, ind.start, size, str);
	else
		append_in_quotes(sys, new, ind, str);
	expand_dollar(sys, new, ind.start);
}

static void	update_cmdarg(t_sys *sys, char **argv, int *index)
{
	char	*str;
	char	*new;
	char	*tofree;

	str = *argv;
	new = NULL;
	tofree = NULL;
	*index = 0;
	while (str[*index])
	{
		if (str[*index] == '\"')
			new_from_dbquote(sys, str, &new, index);
		else if (str[*index] == '\'')
			new_from_squote(sys, str, &new, index);
		else
			append_new_quotes(sys, str, &new, index);
		if (*index == -42)
			break ;
		if (*index < (int)ft_strlen(str))
			*index += 1;
	}
	tofree = *argv;
	*argv = new;
	ft_strdel(&tofree);
	return ;
}

int			handle_expansion(t_sys *sys, t_cmd *cmd)
{
	int		index;
	int		ret;

	index = 0;
	ret = 0;
	if (!cmd->cmd_argv)
		return (0);
	while (cmd->cmd_argv[index])
	{
		ret = 0;
		update_cmdarg(sys, &(cmd->cmd_argv[index]), &ret);
		if (ret == -42)
			return (-1);
		index++;
	}
	return (0);
}
