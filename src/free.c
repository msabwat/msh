/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:15:21 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 14:37:19 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <errno.h>

void	free_lstenv(t_list *cur)
{
	t_list	*next;

	next = NULL;
	while (cur)
	{
		next = cur->next;
		free(cur->content);
		free(cur);
		cur = NULL;
		cur = next;
	}
}

void	free_paths(char **paths)
{
	char	**tofree;

	if (!paths)
		return ;
	tofree = paths;
	while (*paths)
	{
		ft_strdel(&(*paths));
		paths++;
	}
	free(tofree);
}

void	free_cmd(void *node, size_t s)
{
	char	**args;
	char	**ptr;

	(void)s;
	args = ((t_cmd*)node)->cmd_argv;
	ptr = args;
	if (args)
	{
		while (*args)
		{
			ft_strdel(&(*args));
			args++;
		}
		free(ptr);
		ptr = NULL;
	}
	free(node);
	node = NULL;
}

void	free_env(void *node, size_t s)
{
	(void)s;
	if (node)
	{
		ft_strdel(&((t_env*)node)->name);
		ft_strdel(&((t_env*)node)->value);
	}
	free(node);
	node = NULL;
}

void	free_tmp_env(char **env)
{
	while (*env)
	{
		ft_strdel(&(*env));
		env++;
	}
}
