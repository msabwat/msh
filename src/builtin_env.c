/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_env.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:03:33 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/07 21:19:59 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

void	execute_env(t_cmd *cmd, t_list **head)
{
	t_list	*cur;

	cur = *head;
	if (cmd->in != 0)
		return ;
	while (cur)
	{
		if (((t_env*)cur->content)->name && ((t_env*)cur->content)->value)
		{
			ft_putstr_fd(((t_env*)cur->content)->name, cmd->out);
			ft_putstr_fd("=", cmd->out);
			ft_putendl_fd(((t_env*)cur->content)->value, cmd->out);
		}
		cur = cur->next;
	}
	if (cmd->pipe1[1] != 42)
		close(cmd->pipe1[1]);
}
