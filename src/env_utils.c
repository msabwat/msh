/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env_utils.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:12:41 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 12:10:31 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <errno.h>

char		*get_shenv(t_list **lstenv, char *name)
{
	t_list	*cur;

	cur = *lstenv;
	while (cur)
	{
		if (ft_strcmp(((t_env*)cur->content)->name, name) == 0)
			return (((t_env*)cur->content)->value);
		cur = cur->next;
	}
	return (NULL);
}

void		print_lstenv(t_list *cur, t_cmd *cmd)
{
	while (cur)
	{
		if ((((t_env*)cur->content)->name) && (((t_env*)cur->content)->value))
		{
			ft_putstr_fd("declare -x ", cmd->out);
			ft_putstr_fd(((t_env*)cur->content)->name, cmd->out);
			ft_putstr_fd("=", cmd->out);
			ft_putchar_fd('\"', cmd->out);
			ft_putstr_fd(((t_env*)cur->content)->value, cmd->out);
			ft_putchar_fd('\"', cmd->out);
			ft_putstr_fd("\n", cmd->out);
		}
		cur = cur->next;
	}
}

static void	add_env_var(t_sys *sys, t_list **lstenv, char *name, char *value)
{
	t_list	*cur;
	t_env	*node;

	node = NULL;
	node = malloc(sizeof(t_env));
	if (!node)
		exit_with_error(sys, ENOMEM);
	node->name = ft_strnew(ft_strlen(name));
	if (!(node->name))
		exit_with_error(sys, ENOMEM);
	ft_strcpy(node->name, name);
	if (!(node->name))
		exit_with_error(sys, ENOMEM);
	node->value = ft_strnew(ft_strlen(value));
	ft_strcpy(node->value, value);
	cur = *lstenv;
	if (cur == NULL)
		*lstenv = ft_lstnew(node, sizeof(t_env));
	else
	{
		while (cur->next != NULL)
			cur = cur->next;
		ft_lstadd(&cur, ft_lstnew(node, sizeof(t_env)));
	}
}

void		set_shenv(t_sys *sys, t_list **lstenv, char *name, char *value)
{
	t_list	*cur;

	cur = *lstenv;
	if (value == NULL)
		value = "";
	while (cur)
	{
		if (ft_strcmp(((t_env*)cur->content)->name, name) == 0)
		{
			ft_strdel(&(((t_env*)cur->content)->value));
			((t_env*)cur->content)->value = ft_strnew(ft_strlen(value));
			ft_strcpy(((t_env*)cur->content)->value, value);
			break ;
		}
		cur = cur->next;
	}
	if (cur == NULL)
		add_env_var(sys, lstenv, name, value);
}
