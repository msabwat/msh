/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utils.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:16:07 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 12:09:39 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <errno.h>

char	*debug_token(t_tkn_type tkn)
{
	if (tkn == SEMICOLON)
		return ("<semicolon>");
	else if (tkn == PIPE)
		return ("<pipe>");
	else if (tkn == OUTREDIR)
		return ("<outredir>");
	else if (tkn == INREDIR)
		return ("<inredir>");
	else if (tkn == EOFREDIR)
		return ("<eofredir>");
	else if (tkn == SPACE)
		return ("<space>");
	else if (tkn == WORD)
		return ("<word>");
	return ("<default>");
}

char	**update_str(t_sys *sys, t_list *cur, t_token tkn, char *input)
{
	char	**result;
	char	**tmp;
	int		size;
	int		index;

	index = 0;
	tmp = ((t_cmd*)cur->content)->cmd_argv;
	while (tmp[index])
		index++;
	size = index + 1;
	result = malloc(sizeof(char *) * (size + 1));
	if (!result)
		exit_with_error(sys, ENOMEM);
	result[size] = NULL;
	index = 0;
	while (index < size - 1)
	{
		result[index] = tmp[index];
		index++;
	}
	free(tmp);
	result[index] = str_from_tkn(sys, tkn, input);
	return (result);
}

char	*str_from_tkn(t_sys *sys, t_token token, char *input)
{
	char	*new;
	size_t	i;
	size_t	j;

	new = NULL;
	i = 0;
	j = token.start;
	new = ft_strnew(token.end - token.start + 1);
	if (!new)
		exit_with_error(sys, ENOMEM);
	while (i < token.end - token.start + 1)
		new[i++] = input[j++];
	return (new);
}
