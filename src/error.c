/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:12:53 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/11 02:13:21 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include <errno.h>

void	exit_with_error(t_sys *sys, int err)
{
	errno = err;
	ft_putstr_fd("minishell: ", 2);
	ft_putendl_fd(strerror(errno), 2);
	cleanup_shell(sys, errno);
}

void	handle_redir_error(t_list *cur)
{
	((t_cmd*)cur->content)->out = -42;
	ft_putendl_fd("could not redirect: invalid file", 2);
}
