/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_unset.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:11:26 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 11:44:48 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

void	delete_node(t_list **lstenv, t_list *cur, t_list *next)
{
	t_env	*prev;
	t_env	*node;

	prev = (t_env*)cur->content;
	node = (t_env*)next->content;
	if (ft_strcmp(node->name, prev->name) == 0)
	{
		*lstenv = next->next;
		ft_strdel(&(node->name));
		ft_strdel(&(node->value));
		free(node);
		free(next);
	}
	else
	{
		cur->next = next->next;
		ft_strdel(&(node->name));
		ft_strdel(&(node->value));
		free(node);
		free(next);
	}
}

void	execute_unset(t_list **lstenv, char *name, t_cmd *cmd)
{
	t_list	*next;
	t_list	*cur;

	cur = *lstenv;
	next = cur;
	if ((!name) || (!cur) || (cmd->in != 0))
		return ;
	if (ft_strcmp(((t_env*)cur->content)->name, name) == 0)
	{
		delete_node(lstenv, cur, next);
		return ;
	}
	while (cur->next != NULL)
	{
		next = cur->next;
		if (ft_strcmp(((t_env*)next->content)->name, name) == 0)
		{
			delete_node(lstenv, cur, next);
			break ;
		}
		cur = next;
	}
}
