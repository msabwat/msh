/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_pwd.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:11:13 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/11 00:59:12 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <unistd.h>
#include <errno.h>

void	execute_pwd(t_sys *sys, t_cmd *cmd)
{
	char	*buf;

	if (cmd->in != 0)
		return ;
	buf = ft_strnew(PATHMAX);
	if (!buf)
		exit_with_error(sys, ENOMEM);
	if (getcwd(buf, PATHMAX) != NULL)
		ft_putendl_fd(buf, cmd->out);
	else
	{
		ft_putendl_fd("minishell: getcwd failed", 2);
		ft_strdel(&buf);
		cmd->ret = errno;
		return ;
	}
	ft_strdel(&buf);
	if (cmd->pipe1[1] != 42)
		close(cmd->pipe1[1]);
}
