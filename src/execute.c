/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   execute.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:13:09 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 11:46:34 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <unistd.h>
#include <sys/wait.h>
#include <errno.h>

char		*getpath(t_list **lstenv, char **argv)
{
	char		**paths;
	char		*path;
	char		**ptr;
	int			ret;

	path = argv[0];
	paths = ft_strsplit(get_shenv(lstenv, "PATH"), ':');
	ptr = paths;
	ret = 0;
	if (paths)
	{
		path = test_path(&ret, paths, argv[0]);
		if (ret == -1)
		{
			ft_strdel(&path);
			path = argv[0];
		}
		free_paths(ptr);
	}
	return (path);
}

static void	handle_parent_fd(int in, int out, int pipefd1[2], int pipefd2[2])
{
	if ((in == pipefd2[0]) && (out == pipefd1[1]))
	{
		close(in);
		close(out);
	}
	else if ((in != pipefd1[0]) && (out == pipefd1[1]))
		close(out);
	else if ((in == pipefd2[0]) && (out != pipefd2[1]))
		close(in);
}

int			launch(t_sys *sys, t_cmd *cmd, char *path, char **env)
{
	int		id;

	id = fork();
	if (id == 0)
	{
		dup2(cmd->in, 0);
		dup2(cmd->out, 1);
		while (sys->pipes->top != -1)
		{
			close(ft_stackpeek(sys->pipes));
			ft_stackpop(sys->pipes);
		}
		if (execve(path, cmd->cmd_argv, env) == -1)
		{
			ft_putstr_fd("minishell: ", 2);
			ft_putstr_fd(path, 2);
			ft_putendl_fd(": command not found", 2);
			cmd->ret = errno;
			exit(127);
		}
	}
	else if (id > 0)
		handle_parent_fd(cmd->in, cmd->out, cmd->pipe1, cmd->pipe2);
	return (id);
}

char		**str_from_list(t_sys *sys, t_list **lstenv)
{
	size_t	len;
	t_list	*cur;
	char	**res;

	cur = *lstenv;
	len = ft_lstlen(*lstenv);
	res = (char**)malloc(sizeof(char*) * (len + 1));
	if (!res)
		exit_with_error(sys, ENOMEM);
	res[len] = NULL;
	fill_env_str(cur, &res);
	return (res);
}
