/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expand_dbquote.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/02 23:29:11 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 18:32:22 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "minishell.h"
#include "libft.h"
#include <errno.h>

static int			handle_end(char *str, int end, char quote)
{
	if (((str[end] == '\\') && (str[end + 1] == '\\'))
		|| ((str[end] == '\\') && (str[end + 1] == quote)))
		end += 2;
	else
		end++;
	return (end);
}

static char			*new_dqstr(t_sys *sys, char *str, t_indexes ind, char quote)
{
	char	*new;
	int		i;
	int		j;
	int		size;

	i = ind.start;
	j = 0;
	size = ind.end - ind.start;
	if (!(new = ft_strnew(size)))
		exit_with_error(sys, ENOMEM);
	while ((i < (int)ft_strlen(str)) && (j < size))
	{
		if ((str[i] == '\\') && ((str[i + 1] == '\"') || (str[i + 1] == '\\')))
		{
			new[j++] = str[i + 1];
			i += 2;
		}
		else
		{
			if (str[i] == quote)
				break ;
			new[j++] = str[i++];
		}
	}
	return (new);
}

static t_indexes	handle_indexes_db(int *index, char *str, char quote)
{
	int end;
	int start;
	int len;

	start = *index + 1;
	end = start;
	len = (int)ft_strlen(str);
	while (end < len)
	{
		if (str[end] == quote)
			break ;
		end = handle_end(str, end, quote);
	}
	if (str[end] != quote)
		*index = -42;
	else
		*index = end;
	return ((t_indexes){ .start=start, .end=end});
}

void				new_from_dbquote(t_sys *sys, char *str, char **new, int *i)
{
	char		*tofree;
	char		*tmp;
	char		quote;
	t_indexes	ind;

	quote = str[*i];
	tofree = NULL;
	tmp = NULL;
	ind = handle_indexes_db(i, str, quote);
	if (*i == -42)
		return ;
	if (!(*new))
		*new = new_dqstr(sys, str, ind, quote);
	else
	{
		tofree = *new;
		tmp = new_dqstr(sys, str, ind, quote);
		*new = ft_strjoin(tofree, tmp);
		if (!(*new))
			exit_with_error(sys, ENOMEM);
		ft_strdel(&tmp);
		ft_strdel(&tofree);
	}
	expand_dollar(sys, new, ind.start - 1);
}
