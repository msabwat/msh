/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main_utils.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/10 14:32:28 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/11 21:02:56 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <sys/types.h>
#include <sys/wait.h>
#include <signal.h>
#include <errno.h>

void	cleanup_shell(t_sys *sys, int ret)
{
	if (sys)
	{
		if (sys->lstcmd)
			ft_lstdel(&(sys->lstcmd), &free_cmd);
		if (sys->lstenv)
			ft_lstdel(&(sys->lstenv), &free_env);
		if (sys->stackids)
			ft_stackdel(sys->stackids);
		if (sys->pipes)
			ft_stackdel(sys->pipes);
		free(sys);
	}
	exit(ret);
}

void	handle_wait(t_sys *sys, t_cmd *cmd, int id)
{
	int status;

	status = 0;
	if ((cmd->pipe1[1] == cmd->pipe2[0]) && (cmd->pipe1[0] == cmd->pipe2[1]))
	{
		while (sys->stackids->top != -1)
		{
			id = ft_stackpeek(sys->stackids);
			waitpid(id, &status, 0);
			if (WIFSIGNALED(status))
			{
				if (WTERMSIG(status) == SIGQUIT)
					kill(id, SIGQUIT);
				else if (WTERMSIG(status) == SIGINT)
				{
					sys->p_ret = 130;
					sys->state = 1;
				}
			}
			else if (WIFEXITED(status))
				sys->p_ret = WEXITSTATUS(status);
			ft_stackpop(sys->stackids);
		}
	}
}

void	handle_sigint(int sig)
{
	(void)sig;
	ft_putstr("\n");
	ft_putstr("\e[G");
	ft_putstr("\e[2K");
	ft_putstr("$ ");
}

/*
** do nothing in the parent, it will only be handled
** in the child (if any)
*/

void	handle_sigquit(int sig)
{
	(void)sig;
}

void	handle_tokenizer_cur(char **cur, int *index)
{
	char *cursor;

	cursor = *cur;
	if ((*cursor == '\\') && (*(cursor + 1) == '\"'))
	{
		cursor += 2;
		*index += 2;
	}
	else
	{
		cursor += 1;
		*index += 1;
	}
	*cur = cursor;
}
