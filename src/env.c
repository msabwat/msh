/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   env.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:12:22 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 12:48:52 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <errno.h>

static t_env	*alloc_node(t_sys *sys, char **tmp)
{
	t_env	*node;

	node = malloc(sizeof(t_env));
	if (!node)
		exit_with_error(sys, ENOMEM);
	node->name = ft_strnew(ft_strlen(tmp[0]));
	if (!(node->name))
		exit_with_error(sys, ENOMEM);
	ft_strcpy(node->name, tmp[0]);
	node->value = NULL;
	if (tmp[1])
	{
		node->value = ft_strnew(ft_strlen(tmp[1]));
		if (!(node->value))
			exit_with_error(sys, ENOMEM);
		ft_strcpy(node->value, tmp[1]);
	}
	return (node);
}

static t_env	*node_from_env_str(t_sys *sys, char *str_env)
{
	char	**tofree;
	t_env	*node;
	char	**tmp;

	tmp = ft_strsplit(str_env, '=');
	node = NULL;
	tofree = tmp;
	if (tmp)
	{
		node = alloc_node(sys, tmp);
		while (*tmp)
		{
			free(*tmp);
			tmp++;
		}
		free(tofree);
	}
	return (node);
}

t_list			*init_env(t_sys *sys, char **env)
{
	t_list	*ltmp;
	t_env	*node;
	t_list	*lstenv;

	ltmp = NULL;
	lstenv = NULL;
	if (!env)
		return (NULL);
	while (*env)
	{
		node = node_from_env_str(sys, *env);
		if (!lstenv)
			lstenv = ft_lstnew(node, sizeof(t_env));
		else
		{
			ltmp = lstenv;
			while (ltmp->next != NULL)
				ltmp = ltmp->next;
			ft_lstadd(&ltmp, ft_lstnew(node, sizeof(t_env)));
		}
		env++;
	}
	return (lstenv);
}
