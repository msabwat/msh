/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_exit.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:04:03 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 00:29:28 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <stdlib.h>

void	execute_exit(t_sys *sys, t_cmd *cmd, int ret, char **options)
{
	char	*tmp;

	tmp = options[1];
	write(1, "exit\n", 5);
	if (options[2])
	{
		ft_putendl_fd("minishell: exit: too many arguments", 2);
		cmd->ret = 1;
	}
	else if (options[1])
	{
		while (*tmp)
		{
			if (!ft_isdigit(*tmp))
			{
				ft_putendl_fd("minishell: exit: numeric argument required", 2);
				cmd->ret = 2;
				return ;
			}
			tmp++;
		}
		cleanup_shell(sys, ft_atoi(options[1]));
	}
	else
		cleanup_shell(sys, ret);
}
