/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenize.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/12/22 20:01:10 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 18:42:51 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

static t_tkn_type	match_identifier(char c, char next_c, int *index)
{
	t_tkn_type	tkn;

	tkn = UNKNOWN;
	if (c == ';')
		tkn = SEMICOLON;
	else if (c == '|')
		tkn = PIPE;
	else if ((c == ' ') || (c == '\t'))
		tkn = SPACE;
	else if (c == '<')
		tkn = INREDIR;
	else if ((c == '>') && (next_c && (next_c == '>')))
	{
		*index += 2;
		return (EOFREDIR);
	}
	else if (c == '>')
		tkn = OUTREDIR;
	if (tkn != UNKNOWN)
		*index += 1;
	return (tkn);
}

static void			update_index(char *cursor, char **input, int *index)
{
	cursor += 1;
	*index += 1;
	*input = cursor;
}

static t_tkn_type	match_quoted(char quote, char **input, int *index)
{
	char *cursor;

	cursor = *input;
	cursor += 1;
	*index += 1;
	while ((*cursor) && (*cursor != quote))
	{
		if (*cursor == quote)
			break ;
		handle_tokenizer_cur(&cursor, index);
	}
	if (*cursor == '\n')
	{
		update_index(cursor, input, index);
		return (WORD);
	}
	if (*cursor == quote)
	{
		update_index(cursor, input, index);
		return (UNKNOWN);
	}
	*input = cursor;
	return (WORD);
}

static t_tkn_type	match_word(char **input, int *index)
{
	char		*cursor;
	t_tkn_type	tkn;

	cursor = *input;
	tkn = UNKNOWN;
	while ((*cursor) && (!is_separator(*cursor)))
	{
		if ((*cursor == '\"') || (*cursor == '\''))
		{
			tkn = match_quoted(*cursor, &cursor, index);
			*input = cursor;
			if (tkn == WORD)
				return (tkn);
		}
		else
		{
			cursor += 1;
			*index += 1;
			*input = cursor;
		}
	}
	*input = cursor;
	return (WORD);
}

t_token				get_next_token(char **input, int *strt_ind, int *stp_ind)
{
	t_tkn_type	tkn;
	char		*cursor;

	cursor = *input;
	if ((*cursor == '\n') || (*cursor == '\0'))
	{
		*input += 1;
		return ((t_token){.type = UNKNOWN});
	}
	tkn = match_identifier(*cursor, *(cursor + 1), stp_ind);
	if (tkn == UNKNOWN)
		tkn = match_word(&cursor, stp_ind);
	else
		cursor += (*stp_ind - *strt_ind);
	*input = cursor;
	return (init_tkn(tkn, *strt_ind, *stp_ind - 1));
}
