/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   command_utils.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:12:10 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 14:33:15 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <sys/stat.h>
#include <errno.h>

static void	execute_cmd(t_sys *sys, t_list **lstenv, t_cmd *cmd)
{
	char	*path;
	char	**env;
	char	**tmp;
	int		id;

	id = -1;
	path = getpath(lstenv, cmd->cmd_argv);
	env = str_from_list(sys, lstenv);
	tmp = env;
	id = launch(sys, cmd, path, env);
	if (id < 0)
	{
		ft_putendl_fd("fork failed", 2);
		cmd->ret = 130;
		return ;
	}
	ft_stackpush(sys->stackids, id);
	handle_wait(sys, cmd, id);
	free_tmp_env(env);
	free(tmp);
	if (ft_strcmp(path, cmd->cmd_argv[0]) != 0)
		ft_strdel(&path);
}

static void	stat_cmd(struct stat buf, t_sys *sys, t_cmd *cmd, char *path)
{
	if ((buf.st_mode & S_IFMT) == S_IFDIR)
	{
		ft_putstr_fd("minishell: ", cmd->out);
		ft_putstr_fd(path, cmd->out);
		ft_putendl_fd(": Is a directory", cmd->out);
		cmd->ret = 126;
		return ;
	}
	else if (((buf.st_mode & S_IFMT) == S_IFREG)
		&& (buf.st_mode & S_IXUSR))
	{
		execute_cmd(sys, &(sys->lstenv), cmd);
		return ;
	}
	else if (((buf.st_mode & S_IFMT) == S_IFREG)
		&& (!(buf.st_mode & S_IXUSR)))
	{
		ft_putstr_fd("minishell: ", cmd->out);
		ft_putstr_fd(path, cmd->out);
		ft_putendl_fd(": Permission denied", cmd->out);
		cmd->ret = 126;
		return ;
	}
	execute_cmd(sys, &(sys->lstenv), cmd);
}

static void	handle_path(t_sys *sys, t_list **lstenv, t_cmd *cmd)
{
	char		*path;
	int			ret;
	struct stat	buffer;

	path = cmd->cmd_argv[0];
	if ((path[0] == '/') || ((path[0] == '.') && (path[1] == '/')))
	{
		ret = stat(path, &buffer);
		if (ret == 0)
			stat_cmd(buffer, sys, cmd, path);
		else if (ret == -1)
		{
			if (errno == ENOENT)
			{
				ft_putstr_fd("minishell: ", cmd->out);
				ft_putstr_fd(path, cmd->out);
				ft_putendl_fd(": No such file or directory", cmd->out);
			}
			cmd->ret = errno;
			return ;
		}
	}
	else
		execute_cmd(sys, lstenv, cmd);
}

static void	select_cmd(t_sys *sys, char **argv, t_cmd *cmd)
{
	if (!argv)
		return ;
	if (((argv[0]) && (sys->state != 1)) && (cmd->in != -42 && cmd->out != -42))
	{
		if (ft_strcmp(argv[0], "echo") == 0)
			execute_echo(cmd, argv);
		else if (ft_strcmp(argv[0], "cd") == 0)
			execute_cd(cmd, &(sys->lstenv), argv);
		else if (ft_strcmp(argv[0], "pwd") == 0)
			execute_pwd(sys, cmd);
		else if (ft_strcmp(argv[0], "export") == 0)
			execute_export(cmd, sys, argv[1]);
		else if (ft_strcmp(argv[0], "env") == 0)
			execute_env(cmd, &(sys->lstenv));
		else if (ft_strcmp(argv[0], "unset") == 0)
			execute_unset(&(sys->lstenv), argv[1], cmd);
		else if ((ft_strcmp(argv[0], "exit") == 0) && (cmd->in == 0))
			execute_exit(sys, cmd, sys->ret, argv);
		else
			handle_path(sys, &(sys->lstenv), cmd);
	}
	else if (cmd->in == -42 || cmd->out == -42)
		cmd->ret = errno;
}

int			execute(t_sys *sys)
{
	t_cmd	*cmd;
	t_list	*cmd_node;
	int		ret;

	cmd = NULL;
	ret = 0;
	cmd_node = sys->lstcmd;
	while (cmd_node)
	{
		cmd = ((t_cmd *)cmd_node->content);
		ret = handle_expansion(sys, cmd);
		if (ret == -1)
		{
			ft_putendl_fd("syntax error: multiline not supported", 2);
			cmd->ret = 2;
			return (0);
		}
		select_cmd(sys, cmd->cmd_argv, cmd);
		sys->ret = cmd->ret;
		cmd_node = cmd_node->next;
	}
	return (1);
}
