/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tokenize_utils.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:15:56 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 14:20:12 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

t_token	init_tkn(t_tkn_type tkn, int start, int end)
{
	t_token	token;

	token.type = tkn;
	token.start = start;
	token.end = end;
	return (token);
}

void	handle_matched_quote(char **input, int *index)
{
	char	*cursor;

	cursor = *input;
	cursor += 1;
	if (cursor == NULL)
		return ;
	*input = cursor;
	*index += 1;
	while ((*cursor) && (!is_separator(*cursor)))
	{
		if (*cursor == '\n')
			cursor += 1;
		else
		{
			cursor += 1;
			*index += 1;
			*input += 1;
		}
	}
}

int		is_separator(char c)
{
	if ((c == ';') || (c == '|') || (c == '>') || (c == '<')
		|| (c == ' ') || (c == '\t'))
		return (1);
	return (0);
}

int		handle_token1(t_token token, t_token cur_token)
{
	if (cur_token.type != SPACE)
	{
		if (!(validate_token(token, cur_token)))
			return (0);
	}
	if (cur_token.type == UNKNOWN)
		return (0);
	return (1);
}
