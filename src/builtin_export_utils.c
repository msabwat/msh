/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   builtin_export_utils.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/11 02:02:00 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/11 02:19:09 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"

void	set_value_name(t_sys *sys, char **arg)
{
	set_shenv(sys, &(sys->lstenv), arg[0], arg[1]);
	ft_strdel(&arg[0]);
	ft_strdel(&arg[1]);
	free(arg);
}

void	set_value_null(t_sys *sys, char **arg)
{
	set_shenv(sys, &(sys->lstenv), arg[0], NULL);
	ft_strdel(&arg[0]);
	free(arg);
}

void	handle_export_err(t_cmd *cmd, char **arg)
{
	char **tmp;

	tmp = arg;
	ft_putstr_fd("minishell: export: invalid argument", 2);
	ft_putendl_fd("usage: export var=name", 2);
	cmd->ret = 1;
	while (*tmp)
	{
		ft_strdel(&(*tmp));
		tmp++;
	}
	free(arg);
}
