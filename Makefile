#******************************************************************************#
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2020/12/03 00:06:30 by msabwat           #+#    #+#              #
#    Updated: 2021/02/11 02:15:51 by msabwat          ###   ########.fr        #
#                                                                              #
#******************************************************************************#

NAME=minishell

FLAGS=-Wall -Wextra -Werror

CC=clang

INC=-I.

LIBFT_INC=-Ilibft/

SRC_NAME=	tokenize.c \
			tokenize_utils.c \
			parse.c \
			expand.c \
			expand_utils.c \
			expand_squote.c \
			expand_dbquote.c \
			expand_dollar_env.c \
			expand_dollar_ret.c \
			cmd_table.c \
			cmd_table_utils.c \
			execute.c \
			execute_utils.c \
			command_utils.c \
			env.c \
			builtin_echo.c \
			builtin_env.c \
			env_utils.c \
			builtin_export.c \
			builtin_export_utils.c \
			builtin_unset.c \
			builtin_cd.c \
			builtin_pwd.c \
			builtin_exit.c \
			utils.c \
			main_utils.c \
			error.c \
			free.c

OBJ_NAME=$(SRC_NAME:.c=.o)

SRC_PATH=src

OBJ_PATH=.obj

SRC=$(addprefix $(SRC_PATH)/,$(SRC_NAME))

OBJS=$(addprefix $(OBJ_PATH)/,$(OBJ_NAME)) 

TEST=

all: makedir $(NAME)

makedir:
	@mkdir -p .obj

test: FLAGS += -g -fsanitize=address
test: TEST = test
test: makedir $(NAME)
	sh tests/launch_echo.sh ./$(NAME) tests/dbquote > .dbquote
	sh tests/launch_echo.sh ./$(NAME) tests/squote > .squote
	sh tests/launch_echo.sh ./$(NAME) tests/expansion > .expansion
	sh tests/launch.sh ./$(NAME) tests/syntax > .syntax
	diff .dbquote tests/dbquote.ok
	diff .squote tests/squote.ok
	diff .expansion tests/expansion.ok
	diff .syntax tests/syntax.ok

$(NAME): $(OBJ_PATH)/main.o $(OBJS)
	$(MAKE) $(TEST) -C libft
	$(CC) $(FLAGS) $(OBJ_PATH)/main.o $(OBJS) $(LIBFT_INC) $(INC) -L libft/ -lft -o $(NAME)

$(OBJ_PATH)/main.o: main.c
	 $(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c main.c -o $(OBJ_PATH)/main.o 
$(OBJ_PATH)/%.o: $(SRC_PATH)/%.c
	$(CC) $(FLAGS) $(LIBFT_INC) $(INC) -c $< -o $@

clean:
	rm -fr $(NAME)
	rm -fr .dbquote .squote .expansion .syntax

fclean: clean
	rm -fr $(OBJ_PATH)
	rm -fr main.o
	$(MAKE) fclean -C libft

re: fclean all
