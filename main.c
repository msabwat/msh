/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/01/26 02:02:56 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 14:34:56 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"
#include "minishell.h"
#include <signal.h>
#include <unistd.h>
#include <errno.h>

static int		get_return_value(t_list *lstcmd)
{
	while (lstcmd->next != NULL)
		lstcmd = lstcmd->next;
	return ((t_cmd*)lstcmd->content)->ret;
}

static t_sys	*init(char **env)
{
	t_sys	*sys;
	t_list	*lstenv;

	ft_putstr("$ ");
	sys = (t_sys *)malloc(sizeof(t_sys));
	lstenv = init_env(NULL, env);
	if (!sys)
		exit_with_error(sys, ENOMEM);
	sys->lstenv = lstenv;
	sys->lstcmd = NULL;
	sys->stackids = ft_stacknew(MAXSTACK);
	sys->pipes = ft_stacknew(MAXSTACK);
	sys->ret = 0;
	sys->p_ret = 0;
	sys->state = 0;
	signal(SIGQUIT, handle_sigquit);
	signal(SIGINT, handle_sigint);
	return (sys);
}

static void		handle_sys(t_sys *sys)
{
	int ret;

	ret = execute(sys);
	if (ret == 1)
	{
		if (sys->p_ret == 0)
			sys->ret = get_return_value(sys->lstcmd);
		else
		{
			sys->ret = sys->p_ret;
			sys->p_ret = 0;
		}
	}
}

static void		launch_shell(char **str, t_sys *sys)
{
	if (!parse(sys, *str))
	{
		ft_putendl_fd("syntax error", 2);
		sys->ret = 2;
	}
	else
	{
		if (sys->lstcmd)
			handle_sys(sys);
		else
			sys->ret = 0;
	}
	ft_lstdel(&(sys->lstcmd), &free_cmd);
	sys->lstcmd = NULL;
	ft_strdel(&(*str));
	sys->state = 0;
	ft_putstr("$ ");
}

int				main(int ac, char **av)
{
	extern char	**environ;
	char		*str;
	int			res;
	t_sys		*sys;

	(void)ac;
	(void)av;
	str = NULL;
	res = 0;
	sys = init(environ);
	while (42)
	{
		res = get_next_line(0, &str);
		if (res == -1)
			cleanup_shell(sys, -1);
		else if ((res == 0) && (!str))
		{
			ft_putendl_fd("exit", 1);
			break ;
		}
		else if (res > 0)
			launch_shell(&str, sys);
	}
	cleanup_shell(sys, sys->ret);
}
