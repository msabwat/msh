/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_stack.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: msabwat <msabwat@student.42.fr>            +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2021/02/10 13:17:02 by msabwat           #+#    #+#             */
/*   Updated: 2021/02/10 13:17:56 by msabwat          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stddef.h>
#include "libft.h"

t_stack		*ft_stacknew(size_t size)
{
	t_stack	*stack;

	stack = (t_stack*)malloc(sizeof(t_stack));
	if (!stack)
		return (NULL);
	stack->top = -1;
	stack->items = (int*)malloc(sizeof(int) * (size + 1));
	if (!(stack->items))
		return (0);
	stack->items[size] = 0;
	stack->size = size;
	return (stack);
}

void		ft_stackpush(t_stack *stack, int elem)
{
	if (stack == NULL)
		return ;
	if (stack->top == (int)(stack->size - 1))
		return ;
	stack->items[++stack->top] = elem;
}

int			ft_stackpeek(t_stack *stack)
{
	if (stack->top == -1)
		return (-1);
	return (stack->items[stack->top]);
}

int			ft_stackpop(t_stack *stack)
{
	if (stack->top == -1)
		return (-1);
	return (stack->items[stack->top--]);
}

void		ft_stackdel(t_stack *stack)
{
	if (!stack)
		return ;
	if (stack->items)
		free(stack->items);
	stack->items = NULL;
	if (stack)
		free(stack);
	stack = NULL;
}
